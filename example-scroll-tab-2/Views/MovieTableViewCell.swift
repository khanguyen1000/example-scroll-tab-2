//
//  MovieTableViewCell.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 23/01/2024.
//

import UIKit
import SDWebImage

class MovieTableViewCell: UITableViewCell {
    
    static let identifier = "MovieTableViewCell"
    
    private let movieLabel:UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.numberOfLines = 0
        v.font = .systemFont(ofSize: 20, weight: .bold)
        return v
    }()
    
    private let overviewLabel:UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.numberOfLines = 0
        v.font = .systemFont(ofSize: 16, weight: .light)
        v.adjustsFontSizeToFitWidth = false
        v.lineBreakMode = .byTruncatingTail
        return v
    }()
    
    private let releaseDateLabel:UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.numberOfLines = 0
        v.font = .systemFont(ofSize: 16, weight: .bold)
        return v
    }()
    
    private let ratingLabel:UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.numberOfLines = 0
        v.font = .systemFont(ofSize: 16, weight: .bold)
        return v
    }()
    
    private let movieImageView: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFill
        v.translatesAutoresizingMaskIntoConstraints = false
        v.clipsToBounds = true
        return v
    }()


    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    private func setupUI() {
        
        let subInfoStack = UIStackView(arrangedSubviews: [releaseDateLabel, ratingLabel])
        subInfoStack.distribution = .equalSpacing
        subInfoStack.axis = .horizontal
        
        let contentStack = UIStackView(arrangedSubviews: [movieLabel, overviewLabel, subInfoStack])
        contentStack.axis = .vertical
        
        let stack = UIStackView(arrangedSubviews: [movieImageView, contentStack])
        stack.axis = .horizontal
        contentView.addSubview(stack)
        stack.snp.makeConstraints { make in
            make.height.width.equalToSuperview()
        }
        movieImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(0)
            make.width.equalTo(56)
            make.height.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        contentStack.snp.makeConstraints { make in
            make.left.equalTo(movieImageView.snp.right).offset(10)
            make.right.equalToSuperview()
            make.height.equalToSuperview()
        }
        movieLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        overviewLabel.snp.makeConstraints { make in
            make.top.equalTo( movieLabel.snp.bottom)
            make.width.equalToSuperview()
        }
        subInfoStack.snp.makeConstraints { make in
            make.top.equalTo(overviewLabel.snp.bottom)
            make.width.equalToSuperview()
        }
    }
  
    public func configure(with model: MovieTableCellViewModel) {

        guard let url = URL(string: "https://image.tmdb.org/t/p/w500/\(model.imageUrl)") else { return }
        movieLabel.text = model.titleName

        movieImageView.sd_setImage(with: url, completed: nil)
        
        
        
        overviewLabel.text = model.overview
        ratingLabel.text = "views: \(String(model.rating))"
        releaseDateLabel.text = model.releaseDate
    }

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    

}

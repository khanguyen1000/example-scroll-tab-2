//
//  HomeBannerUIView.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 24/01/2024.
//

import UIKit
protocol HomeBannerUIViewDelegate: AnyObject  {
    func goDetailMovieWithId(id: Int)
}

class HomeBannerUIView: UIView {
    private var data: [MovieBannerCellViewModel] = [MovieBannerCellViewModel]()
    weak var delegate: HomeBannerUIViewDelegate?
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 1.5)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let v = UICollectionView(frame: .zero, collectionViewLayout: layout)
        v.register(HomeBannerCollectionViewCell.self, forCellWithReuseIdentifier: HomeBannerCollectionViewCell.identifier)
        v.isPagingEnabled = true
        return v
    }()

    override init(frame: CGRect) {
        let _frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 1.5)
        super.init(frame: _frame)
        addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func configure(data: [MovieBannerCellViewModel]) {
        print("configure")
        self.data = data
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
}

extension HomeBannerUIView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeBannerCollectionViewCell.identifier, for: indexPath) as? HomeBannerCollectionViewCell else { return UICollectionViewCell() }
        let movie = data[indexPath.row]
        
        cell.configure(model: movie)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        
        self.delegate?.goDetailMovieWithId(id: item.id)
    }
}

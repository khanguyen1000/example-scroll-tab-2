//
//  HomeBannerCollectionViewCell.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 24/01/2024.
//

import UIKit

class HomeBannerCollectionViewCell: UICollectionViewCell {
    static let identifier = "HomeBannerCollectionViewCell"
    
    private let bannerImage: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v;
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    func setupUI() {
        addSubview(bannerImage)
        addConstraints()
    }
    
    func addConstraints() {
        bannerImage.snp.makeConstraints { make in
            make.width.height.equalToSuperview()
        }
    }
    
    public func configure(model: MovieBannerCellViewModel) {
        guard let url = URL(string: model.imageUrl) else { return }
        
        bannerImage.sd_setImage(with: url, completed: nil)   
    }
    
    required init(coder: NSCoder) {
        fatalError("HomeBannerCollectionViewCell init fail")
    }
}

//
//  HomeViewController.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 22/01/2024.
//

import UIKit

class HomeViewController: UIViewController {
    weak var delegate: UIBaseTabViewDelegate?
    var data:[Movie] = [Movie]()
    private var headerView: HomeBannerUIView?
    
    private lazy var tableView: UITableView = {
        let v = UITableView()
        v.register(MovieTableViewCell.self, forCellReuseIdentifier: MovieTableViewCell.identifier)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPink
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        setupSubviews()
        setupSwipeGestures()
        fetchData()
    }
    
    private func fetchData() {
        guard let url = Bundle.main.url(forResource: "dummyData", withExtension: "json") else {
                print("Json file not found")
                return
            }
        
        do {
            let data = try Data(contentsOf: url)
            let jsonData = try JSONDecoder().decode(JSONResponseData.self, from: data)
            self.data = jsonData.data
            
            var banner: [MovieBannerCellViewModel] = []
            
            for o in jsonData.data {
                let item = MovieBannerCellViewModel(
                    id: o.id,
                    imageUrl: "https://image.tmdb.org/t/p/w500/\(o.posterPath)"
                )
                banner.append(item)
            }
            
            self.headerView?.configure(data: banner)
        } catch {
            fatalError("fail to fetch data")
        }
    }
    
    private func setupSubviews() {
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        headerView = HomeBannerUIView()
        headerView?.delegate = self
        tableView.tableHeaderView = headerView
    }
    
    private func goDetail(model: MovieDetailViewModel) {
        let vc = MovieDetailViewController()
        vc.configure(model: model)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setupSwipeGestures() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender: )))
        leftSwipe.direction = .left
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        rightSwipe.direction = .right
        
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
            self.delegate?.next()
        } else if sender.direction == .right {
            
        }
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
}

extension HomeViewController: HomeBannerUIViewDelegate {
    func goDetailMovieWithId(id: Int) {
        if let movie = data.first(where: { item in
            item.id == id
        }) {
            let model = MovieDetailViewModel(
                title: movie.title,
                overview: movie.overview,
                imageUrl: movie.posterPath,
                trailerId: movie.trailer,
                releaseDate: movie.releaseDate,
                adult: movie.adult,
                popularity: movie.popularity,
                voteAverage: movie.voteAverage,
                voteCount: movie.voteCount,
                images: movie.images
            )
            
            goDetail(model: model)
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    
}

extension HomeViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        "Top Trending"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = .systemFont(ofSize: 25, weight: .bold)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier, for: indexPath) as? MovieTableViewCell else { return UITableViewCell() }
        
        let movie = data[indexPath.row]
        
        cell.configure(with: MovieTableCellViewModel(
                titleName: movie.title,
                imageUrl: movie.posterPath,
                overview: movie.overview,
                releaseDate: movie.releaseDate,
                rating: movie.voteCount
        ))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = data[indexPath.row]

        let model = MovieDetailViewModel(
            title: movie.title,
            overview: movie.overview,
            imageUrl: movie.posterPath,
            trailerId: movie.trailer,
            releaseDate: movie.releaseDate,
            adult: movie.adult,
            popularity: movie.popularity,
            voteAverage: movie.voteAverage,
            voteCount: movie.voteCount,
            images: movie.images
        )
        
        goDetail(model: model)
    }
    
    
}

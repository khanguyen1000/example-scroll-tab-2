//
//  MainTabBarViewController.swift
//  example-tab-scroller
//
//  Created by Hama Kha on 22/01/2024.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        let vcExplore = ExploreViewController()
        let explore = UINavigationController(rootViewController: vcExplore)
        
        vcExplore.delegate = self
        
        explore.tabBarItem.image = UIImage(systemName: "play.circle")
        explore.title = "Explore"
        
        let vcHome = HomeViewController()
        let home = UINavigationController(rootViewController: vcHome)
        home.tabBarItem.image = UIImage(systemName: "house")
        home.title = "Home"
        
        vcHome.delegate = self
        
        tabBar.tintColor = .label

        setViewControllers([home, explore], animated: false)
    }
}

protocol UIBaseTabViewDelegate: AnyObject {
    func next()
    func back()
}

extension MainTabBarViewController: UIBaseTabViewDelegate {
    func back() {
        print("setupSwipeGestures back")
        self.selectedIndex -= 1
    }
    
    func next() {
        print("setupSwipeGestures next")
        self.selectedIndex += 1
    }
}

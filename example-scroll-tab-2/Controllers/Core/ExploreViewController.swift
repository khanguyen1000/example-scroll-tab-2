//
//  ExploreViewController.swift
//  example-tab-scroller
//
//  Created by Hama Kha on 22/01/2024.
//

import UIKit
import LZViewPager

class ExploreViewController: UIViewController {
    weak var delegate: UIBaseTabViewDelegate?
    
    private let headerView: UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    private let viewPager: LZViewPager = {
        let v = LZViewPager()
        return v
    }()
    
    private var subControllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        view.addSubview(headerView)
        view.addSubview(viewPager)
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self
        
        headerView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.width.equalToSuperview()
            make.height.equalTo(300)
        }
        
        viewPager.snp.makeConstraints{
            $0.top.equalTo(headerView.snp.bottom)
            $0.width.height.equalToSuperview()
            $0.left.equalTo(view.snp.left)
        }
        
        let whatNew = WhatNewViewController()
        whatNew.delegate = self.delegate
        whatNew.title = "What's new"

        let util = UtilViewController()
        util.title = "Util"
        
        subControllers = [whatNew, util]
        viewPager.reload()
    }
}

extension ExploreViewController:LZViewPagerDelegate {
    
}

extension ExploreViewController:LZViewPagerDataSource {
    func numberOfItems() -> Int {
        self.subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        subControllers[index]
    }
    
    func button(at index: Int) -> UIButton {
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        return button
    }
    
}

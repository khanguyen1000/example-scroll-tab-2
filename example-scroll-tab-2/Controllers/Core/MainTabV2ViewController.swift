//
//  MainTabV2ViewController.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 23/01/2024.
//

import UIKit
import LZViewPager

class MainTabV2ViewController: UIViewController {
    private let viewPager: LZViewPager = {
        let v = LZViewPager()
        return v
    }()
    private var subControllers: [UIViewController] = []
    
    private lazy var btnHome: UIButton = {
        let b = UIButton()
        b.setTitleColor(UIColor.white, for: .normal)
        b.setTitle("Home", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
        b.snp.makeConstraints { make in
            make.height.equalTo(56)
        }
        
        b.addTarget(self, action: #selector(btnHomeTab), for: .touchUpInside)
        return b
    }()
    private lazy var btnExplore: UIButton = {
        let b = UIButton()
        b.setTitleColor(UIColor.white, for: .normal)
        b.setTitle("Explore", for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
        b.snp.makeConstraints { make in
            make.height.equalTo(56)
        }
        b.addTarget(self, action: #selector(btnExploreTab), for: .touchUpInside)
        return b
    }()
    
    private lazy var tabButtons: UIStackView = {
        let v = UIStackView(arrangedSubviews: [btnHome, btnExplore])
        v.spacing = 16
        v.axis = .horizontal
        v.distribution = .fillEqually
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        setupUI()
        applyConstraints()
    }
    
    private func setupUI () {
        view.addSubview(tabButtons)
        view.addSubview(viewPager)
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self
        let exploreVC = ExploreViewController()
        let homeVC = HomeViewController()
        subControllers = [homeVC, exploreVC]
        viewPager.reload()
        setActiveButton(index: 0)
    }
    
    
    private func applyConstraints() {
        tabButtons.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.width.equalToSuperview().offset(20)
        }
        viewPager.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view.snp.left)
            make.bottom.equalTo(tabButtons.snp.top)
        }
    }
    
    private func setActiveButton(index: Int) {
        if index == 0{
            btnHome.setTitleColor(UIColor.white, for: .normal)
            btnExplore.setTitleColor(UIColor.gray, for: .normal)
        }
        else if index == 1 {
            btnExplore.setTitleColor(UIColor.white, for: .normal)
            btnHome.setTitleColor(UIColor.gray, for: .normal)
        }
    }
    
    @objc private func btnHomeTab() {
        viewPager.select(index: 0, animated: true)
    }
    
    @objc private func btnExploreTab() {
        viewPager.select(index: 1, animated: true)
    }
}

extension MainTabV2ViewController: LZViewPagerDelegate, LZViewPagerDataSource {
    func heightForHeader() -> CGFloat {
        0
    }
    func numberOfItems() -> Int {
        subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        subControllers[index]
    }
    
    func button(at index: Int) -> UIButton {
        let vc = subControllers[index]
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.setImage(vc.tabBarItem.image, for: .normal)
        button.setTitle("", for: .normal)
        return button
    }
    
    func shouldShowIndicator() -> Bool {
        false
    }
   
    func didSelectButton(at index: Int) {
        setActiveButton(index: index)
    }
    func willTransition(to index: Int) {
        setActiveButton(index: index)
    }
}

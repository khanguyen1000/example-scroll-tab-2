//
//  WhatNewViewController.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 22/01/2024.
//

import UIKit

class WhatNewViewController: UIViewController {
    weak var delegate: UIBaseTabViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        // Do any additional setup after loading the view.
        setupSwipeGestures()
    }
    func setupSwipeGestures() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender: )))
        leftSwipe.direction = .left
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        rightSwipe.direction = .right
        
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
        } else if sender.direction == .right {
            self.delegate?.back()
            
        }
    }
}

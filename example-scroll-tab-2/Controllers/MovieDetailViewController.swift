//
//  MovieDetailViewController.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 24/01/2024.
//

import UIKit
import WebKit

class MovieDetailViewController: UIViewController {
    private let titleLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.font = .systemFont(ofSize: 25, weight: .bold)
        v.numberOfLines = 0
        
        return v
    }()
    
    private let overviewLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.font = .systemFont(ofSize: 18, weight: .regular)
        v.numberOfLines = 0
        
        return v
    }()
    
    private let webview: WKWebView = {
        let v = WKWebView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .systemBackground
        return v
    }()
    
    private let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.showsVerticalScrollIndicator = false
        return v
    }()
    
    private let subsContent: UIStackView = {
        let v = UIStackView()
        v.axis = .vertical
        v.distribution = .fillEqually
        v.spacing = 10
        return v
    }()
    
    private let caroselLabel: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.font = .systemFont(ofSize: 20, weight: .bold)
        v.numberOfLines = 0
        v.text = "Preview images"
        return v
    }()
    private let carousel = HomeBannerUIView()
    
    private let blankView = UIView()
    
    private func generateLabelGroup(label labelText: String, value valueText: String) -> UIStackView {
        let label = UILabel()
        label.text = labelText
        let value = UILabel()
        value.text = valueText
        let v = UIStackView(arrangedSubviews: [label, value])
        v.axis = .horizontal
        
        return v
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        view.backgroundColor = .systemBackground
        // Do any additional setup after loading the view.
        setupUI()
        setupConstraints()
        setupSwipeGestures()
    }
    
    private func setupSwipeGestures() {
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeBack(sender:)))
        rightSwipe.direction = .right
        
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func handleSwipeBack(sender: UISwipeGestureRecognizer) {
            self.navigationController?.popViewController(animated: true)
    }

    
    private func setupUI() {
        view.addSubview(scrollView)
        scrollView.addSubview(carousel)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(overviewLabel)
        scrollView.addSubview(subsContent)
        scrollView.addSubview(caroselLabel)
        scrollView.addSubview(webview)
        scrollView.addSubview(blankView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        
    }
    
    private func setupConstraints() {
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.right.equalToSuperview()
        }

        webview.snp.makeConstraints { make in
            make.top.equalTo(scrollView.snp.top)
            make.width.equalToSuperview()
            make.height.equalTo(300)

        }
        titleLabel.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalTo(webview.snp.bottom).offset(20)
        }
        overviewLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.width.equalToSuperview()
        }
        
        subsContent.snp.makeConstraints { make in
            make.top.equalTo(overviewLabel.snp.bottom).offset(20)
            make.width.equalToSuperview()
        }
        
        caroselLabel.snp.makeConstraints { make in
            make.top.equalTo(subsContent.snp.bottom).offset(20)
            make.width.equalToSuperview()
        }
        
        carousel.snp.makeConstraints { make in
            make.top.equalTo(caroselLabel.snp.bottom).offset(20)
            make.width.equalToSuperview()
            make.height.equalTo(UIScreen.main.bounds.width / 2)
        }
        blankView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalTo(carousel.snp.bottom).offset(20)
            make.height.equalTo(200)
            make.bottom.equalTo(0)
        }
    }
    
    public func configure(model: MovieDetailViewModel) {
        guard let url = URL(string: "https://www.youtube.com/embed/\(model.trailerId)") else {return}
        webview.load(URLRequest(url: url))
        titleLabel.text = model.title
        overviewLabel.text = model.overview
        
        let releaseDateVal = generateLabelGroup(label: "Release Date: ", value: model.releaseDate)
        subsContent.addArrangedSubview(releaseDateVal)
        
        let adultVal = generateLabelGroup(label: "Only Adult: ", value: model.adult ? "Yes": "For everyone")
        subsContent.addArrangedSubview(adultVal)

        let popularityVal = generateLabelGroup(label: "Popularity: ", value: String(model.popularity) )
        subsContent.addArrangedSubview(popularityVal)
        
        let voteAverageVal = generateLabelGroup(label: "Vote average: ", value: String(model.voteAverage))
        subsContent.addArrangedSubview(voteAverageVal)
        
        let voteCountVal = generateLabelGroup(label: "Vote count: ", value: String(model.voteCount))
        subsContent.addArrangedSubview(voteCountVal)
        print(model.images)
        var imagesBanner: [MovieBannerCellViewModel] = []
        for img in model.images {
            let item = MovieBannerCellViewModel(
                id: 0,
                imageUrl: img
            )
            imagesBanner.append(item)
        }
        
        carousel.configure(data: imagesBanner)
    }
}

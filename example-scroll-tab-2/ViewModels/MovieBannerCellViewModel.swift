//
//  MovieBannerCellViewModel.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 24/01/2024.
//

import Foundation

struct MovieBannerCellViewModel {
    let id: Int
    let imageUrl: String
}

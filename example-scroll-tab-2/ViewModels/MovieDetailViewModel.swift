//
//  MovieDetailViewModel.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 24/01/2024.
//

import Foundation

struct MovieDetailViewModel {
    let title, overview, imageUrl, trailerId, releaseDate: String
    let adult: Bool
    let popularity: Double
    let voteAverage: Double
    let voteCount: Int 
    let images: [String]
}

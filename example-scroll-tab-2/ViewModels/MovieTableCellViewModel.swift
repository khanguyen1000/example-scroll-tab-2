//
//  MovieTableCellViewModel.swift
//  example-scroll-tab-2
//
//  Created by Hama Kha on 23/01/2024.
//

import Foundation

struct MovieTableCellViewModel {
    let titleName, imageUrl, overview, releaseDate: String
    let rating: Int
}
